function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// JavaScript Document

/*!
/**
 * Monkey patch jQuery 1.3.1+ to add support for setting or animating CSS
 * scale and rotation independently.
 * https://github.com/zachstronaut/jquery-animate-css-rotate-scale
 * Released under dual MIT/GPL license just like jQuery.
 * 2009-2012 Zachary Johnson www.zachstronaut.com
 */
(function ($) {
  // Updated 2010.11.06
  // Updated 2012.10.13 - Firefox 16 transform style returns a matrix rather than a string of transform functions.  This broke the features of this jQuery patch in Firefox 16.  It should be possible to parse the matrix for both scale and rotate (especially when scale is the same for both the X and Y axis), however the matrix does have disadvantages such as using its own units and also 45deg being indistinguishable from 45+360deg.  To get around these issues, this patch tracks internally the scale, rotation, and rotation units for any elements that are .scale()'ed, .rotate()'ed, or animated.  The major consequences of this are that 1. the scaled/rotated element will blow away any other transform rules applied to the same element (such as skew or translate), and 2. the scaled/rotated element is unaware of any preset scale or rotation initally set by page CSS rules.  You will have to explicitly set the starting scale/rotation value.
  function initData($el) {
    var _ARS_data = $el.data('_ARS_data');

    if (!_ARS_data) {
      _ARS_data = {
        rotateUnits: 'deg',
        scale: 1,
        rotate: 0
      };
      $el.data('_ARS_data', _ARS_data);
    }

    return _ARS_data;
  }

  function setTransform($el, data) {
    $el.css('transform', 'rotate(' + data.rotate + data.rotateUnits + ') scale(' + data.scale + ',' + data.scale + ')');
  }

  $.fn.rotate = function (val) {
    var $self = $(this),
        m,
        data = initData($self);

    if (typeof val == 'undefined') {
      return data.rotate + data.rotateUnits;
    }

    m = val.toString().match(/^(-?\d+(\.\d+)?)(.+)?$/);

    if (m) {
      if (m[3]) {
        data.rotateUnits = m[3];
      }

      data.rotate = m[1];
      setTransform($self, data);
    }

    return this;
  }; // Note that scale is unitless.


  $.fn.scale = function (val) {
    var $self = $(this),
        data = initData($self);

    if (typeof val == 'undefined') {
      return data.scale;
    }

    data.scale = val;
    setTransform($self, data);
    return this;
  }; // fx.cur() must be monkey patched because otherwise it would always
  // return 0 for current rotate and scale values


  var curProxied = $.fx.prototype.cur;

  $.fx.prototype.cur = function () {
    if (this.prop == 'rotate') {
      return parseFloat($(this.elem).rotate());
    } else if (this.prop == 'scale') {
      return parseFloat($(this.elem).scale());
    }

    return curProxied.apply(this, arguments);
  };

  $.fx.step.rotate = function (fx) {
    var data = initData($(fx.elem));
    $(fx.elem).rotate(fx.now + data.rotateUnits);
  };

  $.fx.step.scale = function (fx) {
    $(fx.elem).scale(fx.now);
  };
  /*
  
  Starting on line 3905 of jquery-1.3.2.js we have this code:
  
  // We need to compute starting value
  if ( unit != "px" ) {
      self.style[ name ] = (end || 1) + unit;
      start = ((end || 1) / e.cur(true)) * start;
      self.style[ name ] = start + unit;
  }
  
  This creates a problem where we cannot give units to our custom animation
  because if we do then this code will execute and because self.style[name]
  does not exist where name is our custom animation's name then e.cur(true)
  will likely return zero and create a divide by zero bug which will set
  start to NaN.
  
  The following monkey patch for animate() gets around this by storing the
  units used in the rotation definition and then stripping the units off.
  
  */


  var animateProxied = $.fn.animate;

  $.fn.animate = function (prop) {
    if (typeof prop['rotate'] != 'undefined') {
      var $self,
          data,
          m = prop['rotate'].toString().match(/^(([+-]=)?(-?\d+(\.\d+)?))(.+)?$/);

      if (m && m[5]) {
        $self = $(this);
        data = initData($self);
        data.rotateUnits = m[5];
      }

      prop['rotate'] = m[1];
    }

    return animateProxied.apply(this, arguments);
  };
})(jQuery);
/*
 Color animation 1.6.0
 http://www.bitstorm.org/jquery/color-animation/
 Copyright 2011, 2013 Edwin Martin <edwin@bitstorm.org>
 Released under the MIT and GPL licenses.
*/


'use strict';

(function (d) {
  function h(a, b, e) {
    var c = "rgb" + (d.support.rgba ? "a" : "") + "(" + parseInt(a[0] + e * (b[0] - a[0]), 10) + "," + parseInt(a[1] + e * (b[1] - a[1]), 10) + "," + parseInt(a[2] + e * (b[2] - a[2]), 10);
    d.support.rgba && (c += "," + (a && b ? parseFloat(a[3] + e * (b[3] - a[3])) : 1));
    return c + ")";
  }

  function f(a) {
    var b;
    return (b = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(a)) ? [parseInt(b[1], 16), parseInt(b[2], 16), parseInt(b[3], 16), 1] : (b = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(a)) ? [17 * parseInt(b[1], 16), 17 * parseInt(b[2], 16), 17 * parseInt(b[3], 16), 1] : (b = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(a)) ? [parseInt(b[1]), parseInt(b[2]), parseInt(b[3]), 1] : (b = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(a)) ? [parseInt(b[1], 10), parseInt(b[2], 10), parseInt(b[3], 10), parseFloat(b[4])] : l[a];
  }

  d.extend(!0, d, {
    support: {
      rgba: function () {
        var a = d("script:first"),
            b = a.css("color"),
            e = !1;
        if (/^rgba/.test(b)) e = !0;else try {
          e = b != a.css("color", "rgba(0, 0, 0, 0.5)").css("color"), a.css("color", b);
        } catch (c) {}
        return e;
      }()
    }
  });
  var k = "color backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor outlineColor".split(" ");
  d.each(k, function (a, b) {
    d.Tween.propHooks[b] = {
      get: function get(a) {
        return d(a.elem).css(b);
      },
      set: function set(a) {
        var c = a.elem.style,
            g = f(d(a.elem).css(b)),
            m = f(a.end);

        a.run = function (a) {
          c[b] = h(g, m, a);
        };
      }
    };
  });
  d.Tween.propHooks.borderColor = {
    set: function set(a) {
      var b = a.elem.style,
          e = [],
          c = k.slice(2, 6);
      d.each(c, function (b, c) {
        e[c] = f(d(a.elem).css(c));
      });
      var g = f(a.end);

      a.run = function (a) {
        d.each(c, function (d, c) {
          b[c] = h(e[c], g, a);
        });
      };
    }
  };
  var l = {
    aqua: [0, 255, 255, 1],
    azure: [240, 255, 255, 1],
    beige: [245, 245, 220, 1],
    black: [0, 0, 0, 1],
    blue: [0, 0, 255, 1],
    brown: [165, 42, 42, 1],
    cyan: [0, 255, 255, 1],
    darkblue: [0, 0, 139, 1],
    darkcyan: [0, 139, 139, 1],
    darkgrey: [169, 169, 169, 1],
    darkgreen: [0, 100, 0, 1],
    darkkhaki: [189, 183, 107, 1],
    darkmagenta: [139, 0, 139, 1],
    darkolivegreen: [85, 107, 47, 1],
    darkorange: [255, 140, 0, 1],
    darkorchid: [153, 50, 204, 1],
    darkred: [139, 0, 0, 1],
    darksalmon: [233, 150, 122, 1],
    darkviolet: [148, 0, 211, 1],
    fuchsia: [255, 0, 255, 1],
    gold: [255, 215, 0, 1],
    green: [0, 128, 0, 1],
    indigo: [75, 0, 130, 1],
    khaki: [240, 230, 140, 1],
    lightblue: [173, 216, 230, 1],
    lightcyan: [224, 255, 255, 1],
    lightgreen: [144, 238, 144, 1],
    lightgrey: [211, 211, 211, 1],
    lightpink: [255, 182, 193, 1],
    lightyellow: [255, 255, 224, 1],
    lime: [0, 255, 0, 1],
    magenta: [255, 0, 255, 1],
    maroon: [128, 0, 0, 1],
    navy: [0, 0, 128, 1],
    olive: [128, 128, 0, 1],
    orange: [255, 165, 0, 1],
    pink: [255, 192, 203, 1],
    purple: [128, 0, 128, 1],
    violet: [128, 0, 128, 1],
    red: [255, 0, 0, 1],
    silver: [192, 192, 192, 1],
    white: [255, 255, 255, 1],
    yellow: [255, 255, 0, 1],
    transparent: [255, 255, 255, 0]
  };
})(jQuery); // JavaScript Document

/*
 Shadow animation 1.11
 http://www.bitstorm.org/jquery/shadow-animation/
 Copyright 2011, 2013 Edwin Martin <edwin@bitstorm.org>
 Contributors: Mark Carver, Xavier Lepretre and Jason Redding
 Released under the MIT and GPL licenses.
*/


'use strict';

jQuery(function (h) {
  function r(b, m, d) {
    var l = [];
    h.each(b, function (f) {
      var g = [],
          e = b[f];
      f = m[f];
      e.b && g.push("inset");
      "undefined" !== typeof f.left && g.push(parseFloat(e.left + d * (f.left - e.left)) + "px " + parseFloat(e.top + d * (f.top - e.top)) + "px");
      "undefined" !== typeof f.blur && g.push(parseFloat(e.blur + d * (f.blur - e.blur)) + "px");
      "undefined" !== typeof f.a && g.push(parseFloat(e.a + d * (f.a - e.a)) + "px");

      if ("undefined" !== typeof f.color) {
        var p = "rgb" + (h.support.rgba ? "a" : "") + "(" + parseInt(e.color[0] + d * (f.color[0] - e.color[0]), 10) + "," + parseInt(e.color[1] + d * (f.color[1] - e.color[1]), 10) + "," + parseInt(e.color[2] + d * (f.color[2] - e.color[2]), 10);
        h.support.rgba && (p += "," + parseFloat(e.color[3] + d * (f.color[3] - e.color[3])));
        g.push(p + ")");
      }

      l.push(g.join(" "));
    });
    return l.join(", ");
  }

  function q(b) {
    function m() {
      var a = /^inset\b/.exec(b.substring(c));
      return null !== a && 0 < a.length ? (k.b = !0, c += a[0].length, !0) : !1;
    }

    function d() {
      var a = /^(-?[0-9\.]+)(?:px)?\s+(-?[0-9\.]+)(?:px)?(?:\s+(-?[0-9\.]+)(?:px)?)?(?:\s+(-?[0-9\.]+)(?:px)?)?/.exec(b.substring(c));
      return null !== a && 0 < a.length ? (k.left = parseInt(a[1], 10), k.top = parseInt(a[2], 10), k.blur = a[3] ? parseInt(a[3], 10) : 0, k.a = a[4] ? parseInt(a[4], 10) : 0, c += a[0].length, !0) : !1;
    }

    function l() {
      var a = /^#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(b.substring(c));
      if (null !== a && 0 < a.length) return k.color = [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16), 1], c += a[0].length, !0;
      a = /^#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(b.substring(c));
      if (null !== a && 0 < a.length) return k.color = [17 * parseInt(a[1], 16), 17 * parseInt(a[2], 16), 17 * parseInt(a[3], 16), 1], c += a[0].length, !0;
      a = /^rgb\(\s*([0-9\.]+)\s*,\s*([0-9\.]+)\s*,\s*([0-9\.]+)\s*\)/.exec(b.substring(c));
      if (null !== a && 0 < a.length) return k.color = [parseInt(a[1], 10), parseInt(a[2], 10), parseInt(a[3], 10), 1], c += a[0].length, !0;
      a = /^rgba\(\s*([0-9\.]+)\s*,\s*([0-9\.]+)\s*,\s*([0-9\.]+)\s*,\s*([0-9\.]+)\s*\)/.exec(b.substring(c));
      return null !== a && 0 < a.length ? (k.color = [parseInt(a[1], 10), parseInt(a[2], 10), parseInt(a[3], 10), parseFloat(a[4])], c += a[0].length, !0) : !1;
    }

    function f() {
      var a = /^\s+/.exec(b.substring(c));
      null !== a && 0 < a.length && (c += a[0].length);
    }

    function g() {
      var a = /^\s*,\s*/.exec(b.substring(c));
      return null !== a && 0 < a.length ? (c += a[0].length, !0) : !1;
    }

    function e(a) {
      if (h.isPlainObject(a)) {
        var b,
            e,
            c = 0,
            d = [];
        h.isArray(a.color) && (e = a.color, c = e.length);

        for (b = 0; 4 > b; b++) {
          b < c ? d.push(e[b]) : 3 === b ? d.push(1) : d.push(0);
        }
      }

      return h.extend({
        left: 0,
        top: 0,
        blur: 0,
        spread: 0
      }, a);
    }

    for (var p = [], c = 0, n = b.length, k = e(); c < n;) {
      if (m()) f();else if (d()) f();else if (l()) f();else if (g()) p.push(e(k)), k = {};else break;
    }

    p.push(e(k));
    return p;
  }

  h.extend(!0, h, {
    support: {
      rgba: function () {
        var b = h("script:first"),
            m = b.css("color"),
            d = !1;
        if (/^rgba/.test(m)) d = !0;else try {
          d = m !== b.css("color", "rgba(0, 0, 0, 0.5)").css("color"), b.css("color", m);
        } catch (l) {}
        b.removeAttr("style");
        return d;
      }()
    }
  });
  var s = h("html").prop("style"),
      n;
  h.each(["boxShadow", "MozBoxShadow", "WebkitBoxShadow"], function (b, h) {
    if ("undefined" !== typeof s[h]) return n = h, !1;
  });
  n && (h.Tween.propHooks.boxShadow = {
    get: function get(b) {
      return h(b.elem).css(n);
    },
    set: function set(b) {
      var m = b.elem.style,
          d = q(h(b.elem)[0].style[n] || h(b.elem).css(n)),
          l = q(b.end),
          f = Math.max(d.length, l.length),
          g;

      for (g = 0; g < f; g++) {
        l[g] = h.extend({}, d[g], l[g]), d[g] ? "color" in d[g] && !1 !== h.isArray(d[g].color) || (d[g].color = l[g].color || [0, 0, 0, 0]) : d[g] = q("0 0 0 0 rgba(0,0,0,0)")[0];
      }

      b.run = function (b) {
        b = r(d, l, b);
        m[n] = b;
      };
    }
  });
});
/*! flip - v1.0.15 - 2015-07-23
* https://github.com/nnattawat/flip
* Copyright (c) 2015 Nattawat Nonsung; Licensed MIT */

!function (a) {
  var b = function b(_b, c) {
    _b.data("flipped", !0);

    var e = "rotate" + _b.data("axis");

    _b.find(_b.data("front")).css({
      transform: e + (_b.data("reverse") ? "(-180deg)" : "(180deg)"),
      "z-index": "0"
    }), _b.find(_b.data("back")).css({
      transform: e + "(0deg)",
      "z-index": "1"
    }), _b.one(d(), function () {
      a(this).trigger("flip:done"), void 0 !== c && c.call(this);
    });
  },
      c = function c(b, _c) {
    b.data("flipped", !1);
    var e = "rotate" + b.data("axis");
    b.find(b.data("front")).css({
      transform: e + "(0deg)",
      "z-index": "1"
    }), b.find(b.data("back")).css({
      transform: e + (b.data("reverse") ? "(180deg)" : "(-180deg)"),
      "z-index": "0"
    }), b.one(d(), function () {
      a(this).trigger("flip:done"), void 0 !== _c && _c.call(this);
    });
  },
      d = function d() {
    var a,
        b = document.createElement("fakeelement"),
        c = {
      transition: "transitionend",
      OTransition: "oTransitionEnd",
      MozTransition: "transitionend",
      WebkitTransition: "webkitTransitionEnd"
    };

    for (a in c) {
      if (void 0 !== b.style[a]) return c[a];
    }
  };

  a.fn.flip = function (d, f) {
    return "function" == typeof d && (f = d), this.each(function () {
      var g = a(this);
      if (void 0 === d || "boolean" != typeof d && "string" != typeof d) {
        if (g.data("initiated")) (void 0 !== d.axis || void 0 !== d.reverse) && e.call(this, d, function () {
          g.trigger("flip:change"), void 0 !== f && f.call(this);
        });else {
          g.data("initiated", !0);
          var h = a.extend({
            axis: "y",
            reverse: !1,
            trigger: "click",
            speed: 500,
            forceHeight: !1,
            forceWidth: !1,
            autoSize: !0,
            front: "auto",
            back: "auto"
          }, d);
          "auto" == h.front ? h.front = g.find(".front").length > 0 ? ".front" : "div:first-child" : "autostrict" == h.front && (h.front = "div:first-child"), "auto" == h.back ? h.back = g.find(".back").length > 0 ? ".back" : "div:first-child + div" : "autostrict" == h.back && (h.back = "div:first-child + div"), g.data("reverse", h.reverse), g.data("axis", h.axis), g.data("front", h.front), g.data("back", h.back);
          var i = "rotate" + ("x" == h.axis.toLowerCase() ? "x" : "y"),
              j = 2 * g["outer" + ("rotatex" == i ? "Height" : "Width")]();
          g.find(g.data("back")).css({
            transform: i + "(" + (h.reverse ? "180deg" : "-180deg") + ")"
          }), g.css({
            perspective: j,
            position: "relative"
          });
          var k = h.speed / 1e3 || .5,
              l = g.find(h.front).add(h.back, g);
          if (h.forceHeight ? l.outerHeight(g.height()) : h.autoSize && l.css({
            height: "100%"
          }), h.forceWidth ? l.outerWidth(g.width()) : h.autoSize && l.css({
            width: "100%"
          }), l.css({
            "backface-visibility": "hidden",
            "transform-style": "preserve-3d",
            position: "absolute",
            "z-index": "1"
          }), l.find("*").css({
            "backface-visibility": "hidden"
          }), g.find(g.data("back")).css({
            transform: i + "(" + (h.reverse ? "180deg" : "-180deg") + ")",
            "z-index": "0"
          }), setTimeout(function () {
            l.css({
              transition: "all " + k + "s ease-out"
            }), void 0 !== f && f.call(this);
          }, 20), "click" == h.trigger.toLowerCase()) g.on(a.fn.tap ? "tap" : "click", function (d) {
            d || (d = window.event), g.find(a(d.target).closest('button, a, input[type="submit"]')).length || (g.data("flipped") ? c(g) : b(g));
          });else if ("hover" == h.trigger.toLowerCase()) {
            var m = function m() {
              g.unbind("mouseleave", n), b(g), setTimeout(function () {
                g.bind("mouseleave", n), g.is(":hover") || c(g);
              }, h.speed + 150);
            },
                n = function n() {
              c(g);
            };

            g.mouseenter(m), g.mouseleave(n);
          }
        }
      } else "toggle" == d && (d = !g.data("flipped")), d ? b(g, f) : c(g, f);
    }), this;
  };

  var e = function e(b, c) {
    var d = !1;

    if (void 0 !== b.axis && a(this).data("axis") != b.axis.toLowerCase() && (a(this).data("axis", b.axis.toLowerCase()), d = !0), void 0 !== b.reverse && a(this).data("reverse") != b.reverse && (a(this).data("reverse", b.reverse), d = !0), d) {
      var e = a(this).find(a(this).data("front")).add(a(this).data("back"), a(this)),
          f = e.css("transition");
      e.css({
        transition: "none"
      });
      var g = "rotate" + a(this).data("axis");
      a(this).data("flipped") ? a(this).find(a(this).data("front")).css({
        transform: g + (a(this).data("reverse") ? "(-180deg)" : "(180deg)"),
        "z-index": "0"
      }) : a(this).find(a(this).data("back")).css({
        transform: g + "(" + (a(this).data("reverse") ? "180deg" : "-180deg") + ")",
        "z-index": "0"
      }), setTimeout(function () {
        e.css({
          transition: f
        }), c.call(this);
      }.bind(this), 0);
    } else setTimeout(c.bind(this), 0);
  };
}(jQuery);
/**
 * Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 1.3.5
 */

;

(function (a) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], a);
  } else {
    a(jQuery);
  }
})(function ($) {
  var g = location.href.replace(/#.*/, '');

  var h = $.localScroll = function (a) {
    $('body').localScroll(a);
  };

  h.defaults = {
    duration: 1000,
    axis: 'y',
    event: 'click',
    stop: true,
    target: window
  };

  $.fn.localScroll = function (a) {
    a = $.extend({}, h.defaults, a);

    if (a.hash && location.hash) {
      if (a.target) window.scrollTo(0, 0);
      scroll(0, location, a);
    }

    return a.lazy ? this.on(a.event, 'a,area', function (e) {
      if (filter.call(this)) {
        scroll(e, this, a);
      }
    }) : this.find('a,area').filter(filter).bind(a.event, function (e) {
      scroll(e, this, a);
    }).end().end();

    function filter() {
      return !!this.href && !!this.hash && this.href.replace(this.hash, '') == g && (!a.filter || $(this).is(a.filter));
    }
  };

  h.hash = function () {};

  function scroll(e, a, b) {
    var c = a.hash.slice(1),
        elem = document.getElementById(c) || document.getElementsByName(c)[0];
    if (!elem) return;
    if (e) e.preventDefault();
    var d = $(b.target);
    if (b.lock && d.is(':animated') || b.onBefore && b.onBefore(e, elem, d) === false) return;
    if (b.stop) d._scrollable().stop(true);

    if (b.hash) {
      var f = elem.id === c ? 'id' : 'name',
          $a = $('<a> </a>').attr(f, c).css({
        position: 'absolute',
        top: $(window).scrollTop(),
        left: $(window).scrollLeft()
      });
      elem[f] = '';
      $('body').prepend($a);
      location.hash = a.hash;
      $a.remove();
      elem[f] = c;
    }

    d.scrollTo(elem, b).trigger('notify.serialScroll', [elem]);
  }

  ;
  return h;
});
/**
 * Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 1.4.12
 */


;

(function (a) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], a);
  } else {
    a(jQuery);
  }
})(function ($) {
  var j = $.scrollTo = function (a, b, c) {
    return $(window).scrollTo(a, b, c);
  };

  j.defaults = {
    axis: 'xy',
    duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1,
    limit: true
  };

  j.window = function (a) {
    return $(window)._scrollable();
  };

  $.fn._scrollable = function () {
    return this.map(function () {
      var a = this,
          isWin = !a.nodeName || $.inArray(a.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) != -1;
      if (!isWin) return a;
      var b = (a.contentWindow || a).document || a.ownerDocument || a;
      return /webkit/i.test(navigator.userAgent) || b.compatMode == 'BackCompat' ? b.body : b.documentElement;
    });
  };

  $.fn.scrollTo = function (f, g, h) {
    if (_typeof(g) == 'object') {
      h = g;
      g = 0;
    }

    if (typeof h == 'function') h = {
      onAfter: h
    };
    if (f == 'max') f = 9e9;
    h = $.extend({}, j.defaults, h);
    g = g || h.duration;
    h.queue = h.queue && h.axis.length > 1;
    if (h.queue) g /= 2;
    h.offset = both(h.offset);
    h.over = both(h.over);
    return this._scrollable().each(function () {
      if (f == null) return;
      var d = this,
          $elem = $(d),
          targ = f,
          toff,
          attr = {},
          win = $elem.is('html,body');

      switch (_typeof(targ)) {
        case 'number':
        case 'string':
          if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
            targ = both(targ);
            break;
          }

          targ = win ? $(targ) : $(targ, this);
          if (!targ.length) return;

        case 'object':
          if (targ.is || targ.style) toff = (targ = $(targ)).offset();
      }

      var e = $.isFunction(h.offset) && h.offset(d, targ) || h.offset;
      $.each(h.axis.split(''), function (i, a) {
        var b = a == 'x' ? 'Left' : 'Top',
            pos = b.toLowerCase(),
            key = 'scroll' + b,
            old = d[key],
            max = j.max(d, a);

        if (toff) {
          attr[key] = toff[pos] + (win ? 0 : old - $elem.offset()[pos]);

          if (h.margin) {
            attr[key] -= parseInt(targ.css('margin' + b)) || 0;
            attr[key] -= parseInt(targ.css('border' + b + 'Width')) || 0;
          }

          attr[key] += e[pos] || 0;
          if (h.over[pos]) attr[key] += targ[a == 'x' ? 'width' : 'height']() * h.over[pos];
        } else {
          var c = targ[pos];
          attr[key] = c.slice && c.slice(-1) == '%' ? parseFloat(c) / 100 * max : c;
        }

        if (h.limit && /^\d+$/.test(attr[key])) attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);

        if (!i && h.queue) {
          if (old != attr[key]) animate(h.onAfterFirst);
          delete attr[key];
        }
      });
      animate(h.onAfter);

      function animate(a) {
        $elem.animate(attr, g, h.easing, a && function () {
          a.call(this, targ, h);
        });
      }
    }).end();
  };

  j.max = function (a, b) {
    var c = b == 'x' ? 'Width' : 'Height',
        scroll = 'scroll' + c;
    if (!$(a).is('html,body')) return a[scroll] - $(a)[c.toLowerCase()]();
    var d = 'client' + c,
        html = a.ownerDocument.documentElement,
        body = a.ownerDocument.body;
    return Math.max(html[scroll], body[scroll]) - Math.min(html[d], body[d]);
  };

  function both(a) {
    return $.isFunction(a) || _typeof(a) == 'object' ? a : {
      top: a,
      left: a
    };
  }

  ;
  return j;
});

$(document).ready(function () {
  $.localScroll();
  $(".cache").delay(1000).fadeOut(500);
  $("#wrapper-header").delay(1500).animate({
    opacity: '1',
    width: '100%'
  }, 500);
  $("#wrapper-navbar").delay(2000).animate({
    opacity: '1',
    height: '60px'
  }, 500);
  $("#main-container-image").delay(2500).animate({
    opacity: '1'
  }, 500);
});
/*TRANSITION PAGE*/

var speed = 'slow';
$('html, body').hide();
$(document).ready(function () {
  $('html, body').fadeIn(speed, function () {
    $('a[href], button[href]').click(function (event) {
      var url = $(this).attr('href');
      if (url.indexOf('#') == 0 || url.indexOf('javascript:') == 0) return;
      event.preventDefault();
      $('html, body').fadeOut(speed, function () {
        window.location = url;
      });
    });
  });
});
/* DISABLE HOVER EFFECT WHILE YOU SCROLL FOR A SMOOTHY NAVIGATION */

var body = document.body,
    timer;
window.addEventListener('scroll', function () {
  clearTimeout(timer);
  if (!body.classList.contains('disable-hover')) body.classList.add('disable-hover');
  timer = setTimeout(function () {
    body.classList.remove('disable-hover');
  }, 200);
}, false);
/* BOUTON MENU */

$(document).on('touchstart mouseover', '#stripes', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#stripes").stop().animate({
      scale: '1.2',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#stripes', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#stripes").stop().animate({
      scale: '1',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* MENU SIDE OPEN */

var MENUSIDEOPEN = document.getElementById('stripes');
MENUSIDEOPEN.addEventListener('click', function () {
  $("#main-container-menu").stop().animate({
    left: '0'
  }, 300);
});
/* BOUTON CROSS */

$(document).on('touchstart mouseover', '#cross-menu', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#cross-menu").stop().animate({
      scale: '1.2',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#cross-menu', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#cross-menu").stop().animate({
      scale: '1',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* MENU SIDE CLOSE */

var MENUSIDECLOSE = document.getElementById('cross-menu');
MENUSIDECLOSE.addEventListener('click', function () {
  $("#main-container-menu").stop().animate({
    'left': '-100%'
  }, 300);
});
/* BOUTON MENU ARROW-2 */

$(document).on('touchstart mouseover', '#wrapper-title-2', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#fleche-nav-2").stop().animate({
      rotate: '90deg',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#wrapper-title-2', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#fleche-nav-2").stop().animate({
      rotate: '0deg',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* BOUTON MENU ARROW-3 */

$(document).on('touchstart mouseover', '#wrapper-title-3', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#fleche-nav-3").stop().animate({
      rotate: '90deg',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#wrapper-title-3', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#fleche-nav-3").stop().animate({
      rotate: '0deg',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* BOUTON MENU */

$(document).on('touchstart mouseover', '#stripes', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#stripes").stop().animate({
      scale: '1.2',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#stripes', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#stripes").stop().animate({
      scale: '1',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* BOUTON NEXT */

$(document).on('touchstart mouseover', '#oldnew-next', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#oldnew-next").stop().animate({
      scale: '1.2',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#oldnew-next', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#oldnew-next").stop().animate({
      scale: '1',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
/* BOUTON PREV */

$(document).on('touchstart mouseover', '#oldnew-prev', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#oldnew-prev").stop().animate({
      scale: '1.2',
      opacity: '0.5'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
$(document).on('touchend mouseout', '#oldnew-prev', function (event) {
  event.stopPropagation();
  event.preventDefault();

  if (event.handled !== true) {
    $("#oldnew-prev").stop().animate({
      scale: '1',
      opacity: '1'
    }, 300);
    event.handled = true;
  } else {
    return false;
  }
});
