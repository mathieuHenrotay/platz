const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setResourceRoot('/2eme/projets/platz/public')
   .sass('resources/index.scss', 'public/css')
   .sass('resources/show.scss', 'public/css')
   .babel([
    'resources/js/fastclick.js',
    'resources/js/jquery-animate-css-rotate-scale.js',
    'resources/js/jquery.animate-colors-min.js',
    'resources/js/jquery.animate-shadow-min.js',
    'resources/js/jquery.flip.min.js',
    'resources/js/jquery.localScroll.min.js',
    'resources/js/jquery.scrollTo.min.js',
    'resources/js/main.js'
    ], 'public/js/app.js')
    .copy('resources/img/statics/', 'public/images/');
