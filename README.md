# Platz
## Installation
### lancez les commandes suivantes
```
npm install
composer install
php artisan voyager:install
```
### Décommentez dans `./database/seeds/DatabaseSeeder.php` comme tel  
```
public function run()
{
    //1ere exec
    $this->call(UsersTableSeeder::class);
    $this->call(ThemesTableSeeder::class);

    //2eme exec
    //$this->call(PostsTableSeeder::class);
}  
```
### Lancez la commande  
```  
php artisan db:seed  
```
### Décommentez dans `./database/seeds/DatabaseSeeder.php` comme tel  
```
public function run()
{
    //1ere exec
    //$this->call(UsersTableSeeder::class);
    //$this->call(ThemesTableSeeder::class);

    //2eme exec
    $this->call(PostsTableSeeder::class);
}
```
### Lancez la commande  
```  
php artisan db:seed  
```
