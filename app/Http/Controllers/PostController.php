<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\GenericController;

class PostController extends GenericController

{

  public function homepage(){
    return $this->index([
      'join' => ['themes', 'themes.id', 'theme_id'],
      'addSelect' => ['themes.name', 'themes.image'],
      'orderBy' => [ ['created_at', 'DESC'],['name', 'ASC'] ]
    ]);
  }

}
