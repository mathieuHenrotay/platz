<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Collection;

abstract class GenericController4 extends Controller

{
    // Nom de l'enregistrement [ex: post]
    protected $_r;
    protected $_modele;

    public function __construct(){
      // static::class est du genre 'App\Http\Controller\PostController'
      // Je veux garder juste 'post'
      $this->_r = strtolower(substr(static::class, 21, -10));
      // \App\Post
      $modele = '\\App\\'.ucfirst($this->_r);
      $this->_modele = new $modele;
    }

    /**
     * retourne une liste d'objets
     * @param  array   $join         tables à lier, ex : ['table', 'champ', 'champ']
     * @param  array   $addSelect    éléments à ajouter au select, ex: ['champ',...]
     * @param  array   $where        condition, ex: ['champ', 'condition', 'valeur']
     * @param  array   $orWhere      condition additionnelle (OR), ex: ['champ', 'condition', 'valeur']
     * @param  array   $orderBy      ordinations éventuelles, ex: ['name', 'asc']
     * @param  integer $limit        limitation du nbre de résulats, ex: 2
     * @param  integer $offset       offset, ex: 1
     * @param  array   $eagerLoading autres entités à charger(même nom que dans le modèle), ex: ['theme']
     * @param  string  $view         vue à charger
     * @return View                vue avec le contenu
     */
    public function index(array $mainArray = [])
    {
      GLOBAL $r;
      // je récupère le modèle
      $r = $this->_modele;
      // je définis la vue par défaut
      $view = 'index';
      foreach ($mainArray as $type => $value) {
        switch ($type) {

          case 'join':
          case 'leftJoin':
          case 'rightJoin':
            GLOBAL $r;
            //je teste le type d'élément afin de savoir quelle technique appliquer
            $r = $this->typeTest($value, ['joinType'=>$type]);
            $r = $this->joinTypeSelect($mainArray);
            break;

          case 'where':
          case 'orWhere':
            GLOBAL $r;
            //je teste le type d'élément afin de savoir quelle technique appliquer
            $r = $this->typeTest($value, ['whereType'=>$type]);
            break;

          case 'orderBy':
            GLOBAL $r;
            //je teste le type d'élément afin de savoir quelle technique appliquer
            $r = $this->typeTest($value, ['orderByType'=>$type]);
            break;

          // Si un limit est défini
          case 'limit':
          case 'offset':
            GLOBAL $r;
            $r = $r->$type($value);
            break;

          case 'view':
            $view = $value;
        }
      }

      //je récupère les résultats
      $r = $r->get();

      // je lance les eagerLoading éventuels pour limiter le nombre de requêtes
      if (isset($mainArray['eagerLoading'])) {
        foreach ($eagerLoading as $toLoad) {
          GLOBAL $r;
          $r -> load($toLoad);
        }
      }

      // je retourne la vue avec le contenu
      return View::make($this->_r.'s.'.$view, [$this->_r.'s' => $r]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $r = $this->_modele;
        $r = $r->findOrFail($id);

        // je retourne la vue avec le contenu
        return View::make($this->_r.'s.show', [$this->_r => $r]);
    }

    private function typeTest(array $tab, array $method){
      GLOBAL $r;
      foreach ($method as $key => $value) {
        $type = $key;
        $typeVariant=$value;
      }
      switch (gettype($tab[0])) {
        case 'array':
          foreach ($tab as $array) {
            $r = $this->$type($array, $typeVariant);
          }
          break;

        case 'string':
          $r = $this->$type($tab, $typeVariant);
          break;
      }
      return $r;
    }

    private function joinType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1],'=',$array[2]);
    }

    private function joinTypeSelect(array $array = []){
      GLOBAL $r;
      //Je sélectionne les contenus du modèle et enventuellement d'autres éléments
      $r = $r->select($this->_r.'s.*');
      //si j'ai d'autres élément, je leur mets des alias et je les ajoute
      if (isset($array['addSelect'])) {
        foreach ($array['addSelect'] as $value) {
          $alias = explode('.', $value);
          $alias[1] = ucfirst($alias[1]);
          $alias = implode('', $alias);
          $r = $r->addSelect($value.' as ' .$alias);
        }
      }
      return $r;
    }

    private function whereType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1],$array[2]);
    }

    private function orderByType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1]);
    }

}
