<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Collection;

abstract class GenericController2 extends Controller

{
    // Nom de l'enregistrement [ex: post]
    protected $_r;
    protected $_modele;

    public function __construct(){
      // static::class est du genre 'App\Http\Controller\PostController'
      // Je veux garder juste 'post'
      $this->_r = strtolower(substr(static::class, 21, -10));
      // \App\Post
      $modele = '\\App\\'.ucfirst($this->_r);
      $this->_modele = new $modele;
    }

    /**
     * retourne une liste d'objets
     * @param  array   $join         tables à lier, ex : ['table', 'champ', 'champ']
     * @param  array   $addSelect    éléments à ajouter au select, ex: ['champ',...]
     * @param  array   $where        condition, ex: ['champ', 'condition', 'valeur']
     * @param  array   $orWhere      condition additionnelle (OR), ex: ['champ', 'condition', 'valeur']
     * @param  array   $orderBy      ordinations éventuelles, ex: ['name', 'asc']
     * @param  integer $limit        limitation du nbre de résulats, ex: 2
     * @param  integer $offset       offset, ex: 1
     * @param  array   $eagerLoading autres entités à charger(même nom que dans le modèle), ex: ['theme']
     * @param  string  $view         vue à charger
     * @return View                vue avec le contenu
     */
    public function index(
            array $join=null,
            array $leftJoin=null,
            array $rightJoin=null,
            array $addSelect=[],
            array $where=null,
            array $orWhere=null,
            array $orderBy=null,
            INT $limit = null,
            INT $offset = null,
            array $eagerLoading=[],
            string $view = 'index')
    {
    /*array $join=[ ['themes', 'posts.theme_id', 'themes.id'], ['users', 'posts.user_id', 'users.id'] ],
      array $addSelect=['themes.id', 'themes.name', 'users.name'],
      array $where=[ ['themes.name', 'LIKE', '%shop%'], ['themes.id', '>=', 1] ],
      array $orWhere=['users.name', '=', 'pierre'],
      array $orderBy=[ ['name','asc'], ['themes.id','desc'] ],
      INT $limit = 2,
      INT $offset = 1,
      array $eagerLoading=['theme'],
      string $view = 'index')*/
      GLOBAL $r;
      // je récupère le modèle
      $r = $this->_modele;

      // Si un join est défini
      if ($join) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($join, ['joinType'=>'join']);
        $r = $this->joinTypeSelect($addSelect);
      }

      // Si un join est défini
      if ($leftJoin) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($leftJoin, ['joinType'=>'leftJoin']);
        $r = $this->joinTypeSelect($addSelect);
      }

      // Si un join est défini
      if ($rightJoin) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($rightJoin, ['joinType'=>'rightJoin']);
        $r = $this->selectElements($addSelect);
      }

      // Si un where est défini
      if ($where) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($where, ['whereType'=>'where']);
      }

      // Si un orWhere est défini
      if ($orWhere) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($orWhere, ['whereType'=>'orWhere']);
      }

      // Si un orderBy est défini
      if ($orderBy) {
        GLOBAL $r;
        //je teste le type d'élément afin de savoir quelle technique appliquer
        $r = $this->typeTest($orderBy, ['orderByType'=>'orderBy']);
      }

      // Si un limit est défini
      if ($limit) {
        GLOBAL $r;
        $r = $r->limit($limit);
      }

      // Si un offset est défini
      if ($offset) {
        GLOBAL $r;
        $r = $r->offset($offset);
      }

      //je récupère les résultats
      $r = $r->get();

      // je lance les eagerLoading éventuels pour limiter le nombre de requêtes
      foreach ($eagerLoading as $toLoad) {
        GLOBAL $r;
        $r -> load($toLoad);
      }

      // je retourne la vue avec le contenu
      return View::make($this->_r.'s.'.$view, [$this->_r.'s' => $r]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    private function typeTest(array $tab, array $method){
      GLOBAL $r;
      foreach ($method as $key => $value) {
        $type = $key;
        $typeVariant=$value;
      }
      switch (gettype($tab[0])) {
        case 'array':
          foreach ($tab as $array) {
            $r = $this->$type($array, $typeVariant);
          }
          break;

        case 'string':
          $r = $this->$type($tab, $typeVariant);
          break;
      }
      return $r;
    }

    private function joinType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1],'=',$array[2]);
    }

    private function joinTypeSelect(array $addSelect){
      GLOBAL $r;
      //Je sélectionne les contenus du modèle et enventuellement d'autres éléments
      $r = $r->select($this->_r.'s.*');
      //si j'ai d'autres élément, je leur mets des alias et je les ajoute
      foreach ($addSelect as $value) {
        $alias = explode('.', $value);
        $alias[1] = ucfirst($alias[1]);
        $alias = implode('', $alias);
        $r = $r->addSelect($value.' as ' .$alias);
      }
      return $r;
    }

    private function whereType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1],$array[2]);
    }

    private function orderByType(array $array, string $method){
      GLOBAL $r;
      return $r->$method($array[0],$array[1]);
    }

}
