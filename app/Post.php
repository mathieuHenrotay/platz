<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;



class Post extends Model
{

    public function theme(){
      return $this->belongsTo('App\Theme');
    }

    //quand j'instancie la class, je regarde si je suis dans une route admin et je limite les résultats affichés à l'utilisateur connecté
    protected static function boot(){
      parent::boot();
      if(Request::is('admin/posts*')){
        static::addGlobalScope('post_user', function(Builder $builder){
          $builder->where('user_id', '=', Auth::user()->id);
        });
      }
    }


}
