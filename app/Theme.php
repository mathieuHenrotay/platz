<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class Theme extends Model
{
  public function posts(){
    return $this->hasMany('App\Post');
  }

  //quand j'instancie la class, je regarde si je suis dans une route admin et je limite les résultats affichés à l'utilisateur connecté
  protected static function boot(){
    parent::boot();
    if(Request::is('admin/themes*')){
      static::addGlobalScope('theme_user', function(Builder $builder){
        $builder->where('user_id', '=', Auth::user()->id);
      });
    }
  }
}
