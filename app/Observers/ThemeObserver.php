<?php

namespace App\Observers;

use App\Theme;
use Illuminate\Support\Facades\Auth;

class ThemeObserver
{
    /**
     * Handle the theme "created" event.
     *
     * @param  \App\Theme  $theme
     * @return void
     */
    public function created(Theme $theme)
    {
        $theme->color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        if (isset(Auth::user()->id)) {
          $theme->user_id = Auth::user()->id;
        }
        $theme->save();
    }

    /**
     * Handle the theme "updated" event.
     *
     * @param  \App\Theme  $theme
     * @return void
     */
    public function updated(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "deleted" event.
     *
     * @param  \App\Theme  $theme
     * @return void
     */
    public function deleted(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "restored" event.
     *
     * @param  \App\Theme  $theme
     * @return void
     */
    public function restored(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "force deleted" event.
     *
     * @param  \App\Theme  $theme
     * @return void
     */
    public function forceDeleted(Theme $theme)
    {
        //
    }
}
