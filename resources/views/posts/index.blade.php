@extends('default')

@section('content1')
  <div class="container object">
    <div id="main-container-image">
      <section class="work">
        @foreach ($posts as $post)
          <figure class="white">
            <a href="#">
              <img src="{{ Voyager::image( $post->image ) }}" alt="" />
              <dl>
                <dt>{{ $post->name }}</dt>
                <dd>{{ $post->text }}</dd>
              </dl>
            </a>
            <div id="wrapper-part-info">
              <div class="part-info-image">
                <img src="{{ Voyager::image((json_decode($post->themesImage))[0]->download_link) }}" alt="" width="28" height="28"/>
              </div>
              <div id="part-info">{{ $post->themesName }}</div>
            </div>
          </figure>
        @endforeach
      </section>
    </div>
  </div>

  <div id="wrapper-oldnew">
      <div class="oldnew">
          <div class="wrapper-oldnew-prev">
              <div id="oldnew-prev"></div>
          </div>
          <div class="wrapper-oldnew-next">
              <div id="oldnew-next"></div>
          </div>
      </div>
  </div>
@stop
