
<div id="main-container-menu" class="object">
    <div class="container-menu">

          <div id="main-cross">
            <div id="cross-menu"></div>
          </div>

          <div id="main-small-logo">
            <div class="small-logo"></div>
          </div>
          {!! App::make('App\Http\Controllers\ThemeController')->index(['view'=>'navMobile']) !!}

      </div>
  </div>
