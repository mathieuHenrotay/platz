<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Theme;

class ThemesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

      DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      Theme::truncate();
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');

      $files = [
        '[{"download_link":"themes\\\\May2019\\\\zz3ze2K400KsNv74sjDg.svg","original_name":"icon-psd.svg"}]', '[{"download_link":"themes\\\\May2019\\\\HnG66TbTIFkdPT8VARfZ.svg","original_name":"icon-ai.svg"}]', '[{"download_link":"themes\\\\May2019\\\\yguBlH7uB0BlO5SuT3pG.svg","original_name":"icon-font.svg"}]',
        '[{"download_link":"themes\\\\May2019\\\\fuVggrjyACqi2TtsFSs1.svg","original_name":"icon-themes.svg"}]',
        '[{"download_link":"themes\\\\May2019\\\\97EPvOkx1ITZ5eqy98fs.svg","original_name":"icon-photo.svg"}]',
        '[{"download_link":"themes\\\\May2019\\\\64rS1OXWQ88jaiQGCep0.svg","original_name":"icon-premium.svg"}]'
      ];

      $themes = factory(Theme::class, 6)->create();
      foreach ($themes as $key => $theme) {
        $theme->image = $files[$key];
        $theme->save();
      }

      $this->command->info('entries Created!');


    }
}
