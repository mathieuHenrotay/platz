<?php

use Illuminate\Database\Seeder;

class MenusTableCustomizationsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


      \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      \DB::table('menus')->delete();
      \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \DB::table('menus')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
        ));


    }
}
